//Antonio Silva Paucar

import java.util.Scanner;

public class lab2 {
    
    
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        Scanner scan= new Scanner(System.in);
        BST binary = new BST();
        String input;
        String task;
        int times = scan.nextInt();
        
        
        for(int i = 0; i<=times; i++){
            
            input = scan.nextLine();
            String[] phrases = input.split(" ");
            task = phrases[0];
            
            
            
            switch(task) {
            	case "insert":
            		binary.insert(Integer.parseInt(phrases[1]));// parse as integer since it gave me problems when trying to compareTo
                        //System.out.println(phrases[1]);
            		break;
            	case "inorder":
            		binary.inOrder(binary.getRoot());
                        System.out.println();
            		break;
            	case "preorder":
            		binary.preOrder(binary.getRoot());
                        System.out.println();
            		break;
            	case "postorder":
            		binary.postOrder(binary.getRoot());
                        System.out.println();
            		break;
            	case "delete":
            		binary.delete(Integer.parseInt(phrases[1]));
            		break;
            }
        }
        scan.close();
   
        }
    }

