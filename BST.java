//Antonio Silva Paucar
//extra credit working. final version

public class BST<E extends Comparable<E>> {
    private Node<E> root;

    public BST(){
        root = null;
    }
    // down and slow, focus
    public Node<E> getRoot(){
        return root;
    }


    public void insert(E data){

        // Find the right spot in the tree for the new node
        // Make sure to check if anything is in the tree
        // Hint: if a node n is null, calling n.getData() will cause an error
        //System.out.println(data);
        if(root==null)//if empty add new
        {
            root = new Node(data);
            return;
            
        }

            
            Node<E> temp = root;

            while(true){//traverse the tree to look for the place to add
               int test =0; 
               test = data.compareTo(temp.getData());
               //System.out.println("compareTo " + test);
                
                if(test > 0){
                    //left
                    if(temp.getRightChild()!=null)
                    {
                       temp = temp.getRightChild();
                       //System.out.println("a la right");
                       //test =0;
                    }
                    else{
                       Node<E>a = new Node(data);
                       a.setParent(temp);//set the value of the parent
                       temp.setRightChild(a);
                        //System.out.println("pone a la right");
                       break;
                    }
                
                }
                else if(test < 0)
                {
                    //right
                    if(temp.getLeftChild()!=null)
                    {
                       temp = temp.getLeftChild();
                        //System.out.println("va a la left");
                  
                    }
                    else{
                       Node<E> a = new Node(data);        
                       a.setParent(temp);// keep track of the parent
                       temp.setLeftChild(a);
                       //System.out.println("pone a la left");
                       break;
                    }
                }
                else if(test == 0)
                {
                    //right
                    if(temp.getLeftChild()!=null)
                    {
                       temp = temp.getLeftChild();
                        //System.out.println("va a la left");
                    
                    }
                    else{
                       Node<E> a = new Node(data);        
                       a.setParent(temp);// keep track of the parent
                       temp.setLeftChild(a);
                       //System.out.println("pone a la left");
                       break;
                    }
                }

                         
            }
        
    }
    
   

    public Node<E> find(E data){

        // Search the tree for a node whose data field is equal to data
        if(root==null){
            return null;
        }
        if(root.getData()==data){
            return root;
        }
        
        Node<E> temp = root;
        
        while(temp!=null){//traverse the tree to look
            
            if(temp.getData().compareTo(data)==0){//compare data to find the node where the data is located. if not, return empty
                return temp;
            }
            else{
                if(data.compareTo(temp.getData())==-1){
                   temp=temp.getLeftChild();
                }else{
                   temp=temp.getRightChild();
                }   
            }
        }
        
        return temp;
        
    }

    public void delete(E data){
        // Find the right node to be deleted

        // If said node has no children, simply remove it by setting its parent to point to null instead of it.
        // If said node has one child, delete it by making its parent point to its child.
        // If said node has two children, then replace it with its successor,
        //          and remove the successor from its previous location in the tree.
        // The successor of a node is the left-most node in the node's right subtree.
        // If the value specified by delete does not exist in the tree, then the structure is left unchanged.

        // Hint: You may want to implement a findMin() method to find the successor when there are two children
        
        Node<E> temp = find(data);// first, look for the node contianing the data
        
        
        if(temp==null)
        {
            
       
            return;
        }

        if(temp!=null){//if found, check if: 2 children, 1 children or no children
        Node<E> parent = temp.getParent();
        if(temp.getLeftChild()==null && temp.getRightChild()== null)// checl of has no child
        {
            //System.out.println(" sin hijos");
            if(parent.getRightChild()==temp)// check wich child is calling
            {
                parent.setRightChild(null);
            }
            else
            {
                parent.setLeftChild(null);
            }
            
        }
        else if(temp.getLeftChild()!=null && temp.getRightChild()==null)
        {
            
            //System.out.println("sin hijo derecho");
            if(temp == root)
            {
                root = temp.getLeftChild();
                
            }
            else
            parent.setLeftChild(temp.getLeftChild());
                    
        }
        else if(temp.getLeftChild()==null && temp.getRightChild()!=null)// check if just one child
        {
            //System.out.println("sin hijo izquierdo");
            if(temp == root)
            {
                root = temp.getRightChild();
                
            }
            else
            parent.setRightChild(temp.getRightChild());

        }
        else if(temp.getLeftChild()!=null && temp.getRightChild()!=null){// check when has two children
            
            //System.out.println("con dos hijos");
            Node<E> sucesor = findSucessor(temp);// find the sucessor
            
            
            temp.setData(sucesor.getData());
            //System.out.println(sucesor.getData());
            Node <E>padre = sucesor.getParent();// copy the parent
            if(padre.getLeftChild().getData()==sucesor.getData()){// 
                padre.setLeftChild(null);
            }
            else
            {
                padre.setRightChild(null);
            } 
        }
        
        }
        
    }
    
    public Node<E> findSucessor( Node<E> nodo) // look  for the sucessor by checking if the right child is null, if not, find the min of the rifht tree, if right child is null 
    { 
        if(nodo.getRightChild()!=null){
            return findMin(nodo.getRightChild());
        }
        
        Node <E> temp = nodo.getParent();// find the parent
        while(temp!= null && nodo==temp.getRightChild()){
            nodo = temp;
            temp = temp.getParent();
        }
        return temp;
    } 
    
    public Node<E> findMin(Node a){//traverse the tree to the extreme left to find the min
        Node<E> temp = a;
        
        while(temp.getLeftChild()!=null){
            temp = temp.getLeftChild();
        }
        
        return temp;
        
    }
    
    

    public void traverse(String order, Node top) {
        if (top != null){
            switch (order) {
                case "preorder":
                    // Your Code here
                    preOrder(top);
                    break;
                case "inorder":
                    // Your Code here
                    inOrder(top);
                    break;
                case "postorder":
                    // Your Code here
                    postOrder(top);
                    break;
            }
        }
    }
    // methods added for getting the transverals
    public void inOrder(Node a){//recursive methods to print
        if(a == null)
            return;
        
        inOrder(a.getLeftChild());// in order is left, root, right
        System.out.print(a.getData()+" ");
        inOrder(a.getRightChild());
        
        
    }
    public void postOrder(Node a){
        if(a == null)
            return;
        
        postOrder(a.getLeftChild());// post order is left, root, 
        postOrder(a.getRightChild());
        System.out.print(a.getData()+" ");
        
    }
    public void preOrder(Node a){
        if(a == null)
            return;
        
        System.out.print(a.getData()+" ");//root, left right
        preOrder(a.getLeftChild());
        preOrder(a.getRightChild());

    }
   
    
  

}
