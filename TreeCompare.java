
import java.util.Scanner;
 
public class TreeCompare<E extends Comparable<E>> {
     
        public static void main(String[] args) throws Exception{
        // TODO code application logic here
        Scanner scan= new Scanner(System.in);
        BST binary = new BST();
        BST second = new BST();
        String input;
        String task;
		
		
		int times = scan.nextInt();

        for(int i = 0; i<=times; i++){
            
            input = scan.nextLine();
            String[] phrases = input.split(" ");
            task = phrases[0];

            switch(task) {
            	case "insert":
            		binary.insert(Integer.parseInt(phrases[1]));
                       //System.out.println(phrases[1]);
            		break;
            	case "inorder":
            		binary.inOrder(binary.getRoot());
                        System.out.println();
            		break;
            	case "preorder":
            		binary.preOrder(binary.getRoot());
                        System.out.println();
            		break;
            	case "postorder":
            		binary.postOrder(binary.getRoot());
                        System.out.println();
            		break;
            	case "delete":
            		binary.delete(Integer.parseInt(phrases[1]));
            		break;
            }
        }
 
         
        int times2 = scan.nextInt();
         
         
        for(int i = 0; i<=times2; i++){
            
            input = scan.nextLine();
            String[] phrases = input.split(" ");
            task = phrases[0];
            
            
            
            switch(task) {
            	case "insert":
            		binary.insert(Integer.parseInt(phrases[1]));
                        //System.out.println(phrases[1]);
            		break;
            	case "inorder":
            		binary.inOrder(binary.getRoot());
                        System.out.println();
            		break;
            	case "preorder":
            		binary.preOrder(binary.getRoot());
                        System.out.println();
            		break;
            	case "postorder":
            		binary.postOrder(binary.getRoot());
                        System.out.println();
            		break;
            	case "delete":
            		binary.delete(Integer.parseInt(phrases[1]));
            		break;
            }
        }
         
         
        if(checkShape(binary.getRoot(), second.getRoot())){
            System.out.println("The trees have the same shape.");
        }
        else
        {
            System.out.println("The trees do not have the same shape.");
             
        }
           
        scan.close();
 
        }
         
 
         
        public static boolean checkShape(Node first, Node second){
             
            if(first == null && second == null)
                return true;// return true if both trees are empty
             
            if(first != null && second != null){
                return checkShape(first.getLeftChild(), second.getLeftChild()) &&  checkShape(first.getRightChild(), second.getRightChild());
            }// it checks node by node at the same time. The way it checks if they are the same tree (isomorphism) is by checking if a node is present in the same position in the first three and in the second.
             // It doesn't check if values are the same, just if there exists pointers at the same position in different trees.
            return false;
        }
 
     
}